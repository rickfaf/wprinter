module.exports = {
  preset: "ts-jest",
  testEnvironment: "jsdom",
  // testEnvironment: "node",
  // transformIgnorePatterns: ,
  transform: {
    "^.+\\.ts$": "ts-jest",
  },
  moduleFileExtensions: [
    "js",
    "json",
    "ts",
  ],
  testMatch: [
    "**/(src|tests)/*.spec.(js|ts)",
  ],

  
}