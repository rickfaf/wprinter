# Wprinter

Print on thermal printer from a web app.

## How to use Wprinter

Add @wprinter to the project:
```
npm install wprinter
```

Create a file src/main.js with the following content:
```
import { printer } from 'wprinter'

const printerInstance = printer('usb')

await printerInstance.printText([
  '',
  printerInstance.textCenter('TEXT ==> '),
  '',
  // printer.LINE_BREAK,
])
```

## Contribute

### Install and build

```sh
# Install once all Node.js dependencies
npm install

# build ts files
npm run tsc:watch
```