export type ConnectionType = 'both' | 'usb' | 'bluetooth'
export interface CanvasDimensions {
  width: number
  height: number
}

export interface FilterOptions {
  vendorId: number
}