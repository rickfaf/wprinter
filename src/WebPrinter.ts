/// <reference types="web-bluetooth" />
import deburr from 'lodash/deburr'
import { CanvasDimensions, ConnectionType, FilterOptions } from './types'


export default class WebPrinter {
  private _connectionType: ConnectionType
  private _printerLineLength: number
  private _printerCharacteristic: any
  private _endpointOut: number = 0
  private _endpointIn: number = 0
  private _usbDevicesFilter: FilterOptions[]

  private _index = 0
  private _data: Uint8ClampedArray | Uint8Array = new Uint8Array([])
  private _imageData: Uint8ClampedArray | Uint8Array = new Uint8Array([])
  private _canvas: CanvasDimensions = {
    width: 120,
    height: 120
  }

  constructor (
      connectionType: ConnectionType = 'both',
      printerLineLength = 32,
      printerCharacteristic = null,
      usbDevicesFilter = []
    ) {

    this._connectionType = connectionType
    this._printerLineLength = printerLineLength
    this._printerCharacteristic = printerCharacteristic,
    this._usbDevicesFilter = usbDevicesFilter
  }

   // Printer CONNECTION ==================================>
  private async _connectToDevice (connectionType: ConnectionType) {
    try {
      if (connectionType === 'usb') {
        const response = await this._connectWithUsb()

        this._printerCharacteristic = response.usbDevice
        this._endpointOut = response.endpointOut
        this._endpointIn = response.endpointIn
        
      } else if (connectionType === 'bluetooth') {
        this._printerCharacteristic = await this._connectWithBluetooth()
      
      } else {
        try {
          this._printerCharacteristic = await this._connectWithBluetooth()
          this._connectionType = 'bluetooth'

        } catch (error) {
          console.error('Error failed connectWithBluetooth ==> ', error)
          console.log('Trying to connect via usb ...')
          const response = await this._connectWithUsb()
          
          this._printerCharacteristic = response.usbDevice
          this._endpointOut = response.endpointOut
          this._endpointIn = response.endpointIn
          this._connectionType = 'usb'
        }
      }

    } catch (error) {
      throw new Error(`connectToDevice ==> ${error}`)
    }
  }

  private async _connectWithBluetooth () {
    try {
      const device = await navigator.bluetooth.requestDevice({
        // acceptAllDevices: true,
        // optionalServices: ['000018f0-0000-1000-8000-00805f9b34fb'],
        filters: [{
          services: ['000018f0-0000-1000-8000-00805f9b34fb']
        }]
      })
      
      if (!device.gatt) {
        throw new Error('connectToDevice ==> device.gatt is undefined')
      }

      const server = await device.gatt.connect()
      // console.log('server ==> ', server)
      const service = await server.getPrimaryService("000018f0-0000-1000-8000-00805f9b34fb")
      // console.log('service ==> ', service)
      const characteristic = await service.getCharacteristic("00002af1-0000-1000-8000-00805f9b34fb")
      console.log('Connected Successfully to device ==>')
      // console.log('DONE ==> ', characteristic)

      return characteristic

    } catch (error) {
      throw new Error(`connectToDevice ==> ${error}`)
    }
  }

  private async _connectWithUsb () {
    try {
      const filters = [
        { vendorId:1046 },
        { vendorId:1208 },
      ]
      if (this._usbDevicesFilter.length) {
        filters.push(...this._usbDevicesFilter)
      }

      const usbDevice = await navigator.usb.requestDevice({
        filters
      })
  
      // console.log('usbDevice  ==> ', usbDevice)

      if (!usbDevice.configuration) {
        throw new Error('connectToDevice ==> usbDevice.configuration is undefined')
      }

      const configurationValue = usbDevice.configuration.configurationValue
      // console.log('configurationValue ==> ', configurationValue)

      if (!usbDevice.configuration.interfaces || !usbDevice.configuration.interfaces.length) {
        throw new Error('connectToDevice ==> usbDevice.configuration is undefined')
      }

      const interfaceNumber = usbDevice.configuration.interfaces[0].interfaceNumber
      // console.log('interfaceNumber ==> ', interfaceNumber)
  
      let endpoints: USBEndpoint[] | null = null
      if (usbDevice.configuration.interfaces[0].alternate 
        && usbDevice.configuration.interfaces[0].alternate.endpoints
        && usbDevice.configuration.interfaces[0].alternate.endpoints.length > 1) {

        endpoints = usbDevice.configuration.interfaces[0].alternate.endpoints
        

      } else if (usbDevice.configuration.interfaces[0].alternates 
        && usbDevice.configuration.interfaces[0].alternates[0]
        && usbDevice.configuration.interfaces[0].alternates[0].endpoints
        && usbDevice.configuration.interfaces[0].alternates[0].endpoints.length > 1) {

        endpoints = usbDevice.configuration.interfaces[0].alternates[0].endpoints
      }

      if (!endpoints) {
        throw new Error('connectToDevice ==> endpoints not found')
      }

      let endpointOut: number = 3, endpointIn: number = 1
      endpoints.map(e => {
        if (e.direction === 'out') {
          endpointOut = e.endpointNumber
        } else if (e.direction === 'in') {
          endpointIn = e.endpointNumber
        }
      })
      // console.log('endpointOut ==> ', endpointOut)
      // console.log('endpointIn ==> ', endpointIn)
      
  
      await usbDevice.open()
      await usbDevice.selectConfiguration(configurationValue)
      await usbDevice.claimInterface(interfaceNumber)

      if (usbDevice.vendorId === 1208) {
        this._printerLineLength = 48
      } else {
        this._printerLineLength = 32
      }
      // console.log('USB DEVICE END => ', usbDevice)
  
      return {
        usbDevice,
        endpointOut,
        endpointIn
      }

    } catch (error) {
      throw new Error(`connectWithUsb ==> ${error}`)
    }
  }
  // Printer CONNECTION ==================================>


  // Print TEXT ==================================>
  async printText (lines: string[]) {
    try {
      const encoder = new TextEncoder()
      const joinedText = lines.join(this.LINE_BREAK)
       // Print
      for (let i = 0; i < joinedText.length; i += 19) {
        const text = joinedText.substr(i, 19)
        // console.log('Text to print ==> ', text)
        let textUint8Array = encoder.encode(text)

        if (i === 0) {
          // Increase text size
          const newBuffer: number[] = []
          textUint8Array.forEach(i => newBuffer.push(i))
          const hexs = this._FONT_SIZE.bold
          newBuffer.unshift(...hexs)
          textUint8Array = new Uint8Array(newBuffer)
        }

        await this._writeValueToPrint(textUint8Array)
      }
      // Print

      return {
        printerCharacteristic: this._printerCharacteristic,
        connectionType: this._connectionType
      }

    } catch (error) {
      throw new Error(`printValue ==> ${error}`)
    }
  }
  // Print TEXT ==================================>



  // Print IMAGE ==================================>
  public async printImage (imageData: Uint8Array | Uint8ClampedArray, width: number, height: number) {
    try {
      this._imageData = imageData
      this._canvas.width = width
      this._canvas.height = height
      await this._sendImageData()

      return {
        printerCharacteristic: this._printerCharacteristic,
        connectionType: this._connectionType
      }

    } catch (error) {
      throw new Error(`printImage ==> ${error}`)
    }
  }
  private async _sendImageData() {
    this._index = 0
    this._data = this._getImagePrintData()

    return new Promise((resolve, reject) => {
      this._sendNextImageDataBatch(resolve, reject)
    })
  }
  private _sendNextImageDataBatch(resolve, reject) {
    // Can only write 512 bytes at a time to the characteristic
    // Need to send the image data in 512 byte batches
    if (this._index + 64 < this._data.length) {
      this._writeValueToPrint(this._data.slice(this._index, this._index + 64)).then(() => {
        this._index += 64
        this._sendNextImageDataBatch(resolve, reject)
      }).catch(error => reject(error))
      
    } else {
      // Send the last bytes
      if (this._index < this._data.length) {
        this._writeValueToPrint(this._data.slice(this._index, this._data.length)).then(() => {
          resolve()
        }).catch(error => reject(error))
        
      } else {
        resolve()
      }
    }
  }
  private async _writeValueToPrint (data: Uint8Array | Uint8ClampedArray) {
    if (this._connectionType === 'bluetooth') {
      if (!this._printerCharacteristic) {
        await this._connectToDevice(this._connectionType)
      }
      await this._printerCharacteristic.writeValue(data)
      
    } else if (this._connectionType === 'usb') {
      if (!this._endpointOut || !this._endpointIn || !this._printerCharacteristic) {
        await this._connectToDevice(this._connectionType)
      }
      await this._printerCharacteristic.transferOut(this._endpointOut, data)

    } else {
      await this._connectToDevice(this._connectionType)
      await this._writeValueToPrint(data)
    }
  }
  private _getImagePrintData() {
    // if (this._imageData == null) {
    //   console.error('No image to print!')
    //   return new Uint8Array([])
    // }

    // Each 8 pixels in a row is represented by a byte
    let printData = new Uint8Array(this._canvas.width / 8 * this._canvas.height + 8)
    let offset = 0
    // Set the header bytes for printing the image
    printData[0] = 29  // Print raster bitmap
    printData[1] = 118 // Print raster bitmap
    printData[2] = 48 // Print raster bitmap
    printData[3] = 0  // Normal 203.2 DPI
    printData[4] = this._canvas.width / 8 // Number of horizontal data bits (LSB)
    printData[5] = 0 // Number of horizontal data bits (MSB)
    printData[6] = this._canvas.height % 256 // Number of vertical data bits (LSB)
    printData[7] = this._canvas.height / 256  // Number of vertical data bits (MSB)
    offset = 7
    // Loop through image rows in bytes
    for (let i = 0; i < this._canvas.height; ++i) {
      for (let k = 0; k < this._canvas.width / 8; ++k) {
        let k8 = k * 8
        //  Pixel to bit position mapping
        printData[++offset] = this._getDarkPixel(k8 + 0, i) * 128 + this._getDarkPixel(k8 + 1, i) * 64 +
        this._getDarkPixel(k8 + 2, i) * 32 + this._getDarkPixel(k8 + 3, i) * 16 +
        this._getDarkPixel(k8 + 4, i) * 8 + this._getDarkPixel(k8 + 5, i) * 4 +
        this._getDarkPixel(k8 + 6, i) * 2 + this._getDarkPixel(k8 + 7, i)
      }
    }

    return printData
  }
  private _getDarkPixel(x, y) {
    // Return the pixels that will be printed black
    let red = this._imageData[((this._canvas.width * y) + x) * 4];
    let green = this._imageData[((this._canvas.width * y) + x) * 4 + 1];
    let blue = this._imageData[((this._canvas.width * y) + x) * 4 + 2];
    return (red + green + blue) > 0 ? 1 : 0;
  }
  // Print IMAGE ==================================>


  // private _handleError (message: string, error: any = '') {
  //   console.error(`Error ${message} ==> `, error)
  //   throw new Error(`Error ${message} ==> ${error}`)
  // }


  // Printer utils
  public spaceBetween (start: string, end: string) {
    return start.padEnd(this._printerLineLength - end.length) + end
  }
  public line (char = '-') {
    return ''.padEnd(this._printerLineLength, char)
  }
  public textCenter (text: string, char = ' ') {
    text = deburr(text)
    const leftLength = (this._printerLineLength - text.length) / 2
    return text.padStart(leftLength + text.length, char).padEnd(this._printerLineLength, char)
  }

  public LINE_BREAK = '\n'
  private _FONT_SIZE = {
    default: [0x1b, 0x21, 0x00],
    smallFont: [0x1b, 0x21, 0x01],
    bold: [0x1b, 0x21, 0x08],
    doubleHeight: [0x1b, 0x21, 0x10],
    doubleWidth: [0x1b, 0x21, 0x20],
    doubleHeightAndWidth: [0x1b, 0x21, 0x20]
  }
  // Printer utils

}