import { ConnectionType } from "./types"
import WebPrinter from "./WebPrinter"

// export function test () {
//   console.log('test 2 ==>')
// }

export function printer (connectionType: ConnectionType = 'both', printerLineLength = 32,
printerCharacteristic = null) {
  return new WebPrinter(connectionType, printerLineLength, printerCharacteristic)
}
